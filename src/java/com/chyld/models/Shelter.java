package com.chyld.models;

import org.hibernate.annotations.BatchSize;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "shelters")
@Access(AccessType.PROPERTY)
public class Shelter {
    private int id;
    private String name;
    private Date opened;
    private List<Animal> animals;

    public Shelter() {
    }

    public Shelter(String name, Date opened) {
        this.name = name;
        this.opened = opened;
        this.animals = new ArrayList<>();
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    public int getId() {return id;}
    public void setId(int id) {this.id = id;}

    @Basic
    @Column(name = "name", unique = true, nullable = false, length = 45)
    public String getName() {return name;}
    public void setName(String name) {this.name = name;}

    @Basic
    @Column(name = "opened")
    @Temporal(TemporalType.DATE)
    public Date getOpened() {return opened;}
    public void setOpened(Date opened) {this.opened = opened;}

    @OneToMany(mappedBy = "shelter")
    @Fetch(FetchMode.SELECT)
    @BatchSize(size = 10)
    public List<Animal> getAnimals() {return animals;}
    public void setAnimals(List<Animal> animals) {this.animals = animals;}
}
