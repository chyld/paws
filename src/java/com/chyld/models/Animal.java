package com.chyld.models;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "animals")
@Access(AccessType.PROPERTY)
public class Animal {
    private int id;
    private String name;
    private int age;
    private String sex;
    private Date birthday;
    private String species;
    private Shelter shelter;

    public Animal() {
    }

    public Animal(String name, int age, String sex, Date birthday, String species, Shelter shelter) {
        this.name = name;
        this.age = age;
        this.sex = sex;
        this.birthday = birthday;
        this.species = species;
        this.shelter = shelter;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    public int getId() {return id;}
    public void setId(int id) {this.id = id;}

    @Basic
    @Column(name = "name", length = 45)
    public String getName() {return name;}
    public void setName(String name) {this.name = name;}

    @Basic
    @Column(name = "age")
    public int getAge() {return age;}
    public void setAge(int age) {this.age = age;}

    @Basic
    @Column(name = "sex", length = 1)
    public String getSex() {return sex;}
    public void setSex(String sex) {this.sex = sex;}

    @Basic
    @Column(name = "birthday")
    @Temporal(TemporalType.DATE)
    public Date getBirthday() {return birthday;}
    public void setBirthday(Date birthday) {this.birthday = birthday;}

    @Basic
    @Column(name = "species", length = 45)
    public String getSpecies() {return species;}
    public void setSpecies(String species) {this.species = species;}

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "shelter_id", referencedColumnName = "id")
    public Shelter getShelter() {return shelter;}
    public void setShelter(Shelter shelter) {this.shelter = shelter;}
}
