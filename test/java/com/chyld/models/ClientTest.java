package com.chyld.models;

import com.chyld.util.Mysql;
import org.hibernate.Session;
import org.hibernate.exception.ConstraintViolationException;
import org.hibernate.query.NativeQuery;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import org.hamcrest.CoreMatchers;

import java.text.SimpleDateFormat;

public class ClientTest {
    @Before
    public void setUp() throws Exception {
        Session session = Mysql.getSession();
        session.beginTransaction();
        session.createNativeQuery("SET FOREIGN_KEY_CHECKS = 0").executeUpdate();
        session.createNativeQuery("truncate table clients").executeUpdate();
        session.createNativeQuery("SET FOREIGN_KEY_CHECKS = 1").executeUpdate();
        Client client = new Client("Sue");
        session.save(client);
        session.getTransaction().commit();
        session.close();
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void testCreateBasicClient() throws Exception {
        Session session = Mysql.getSession();
        session.beginTransaction();
        Client client = new Client("Sam");
        session.save(client);
        session.getTransaction().commit();
        session.close();
        assertEquals(2, client.getId());
    }
}
