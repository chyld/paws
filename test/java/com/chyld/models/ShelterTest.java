package com.chyld.models;

import com.chyld.util.Mysql;
import org.hibernate.Session;
import org.hibernate.exception.ConstraintViolationException;
import org.hibernate.query.NativeQuery;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import org.hamcrest.CoreMatchers;

import java.text.SimpleDateFormat;

public class ShelterTest {
    private SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");

    @Before
    public void setUp() throws Exception {
        Session session = Mysql.getSession();
        session.beginTransaction();
        session.createNativeQuery("SET FOREIGN_KEY_CHECKS = 0").executeUpdate();
        session.createNativeQuery("truncate table shelters").executeUpdate();
        session.createNativeQuery("truncate table animals").executeUpdate();
        session.createNativeQuery("SET FOREIGN_KEY_CHECKS = 1").executeUpdate();
        Shelter shelter =  new Shelter("Happi Tails", format.parse("2012-01-15"));
        Animal fido = new Animal("fido", 3, "m", format.parse("2016-03-10"), "dog", shelter);
        session.save(fido);
        session.getTransaction().commit();
        session.close();
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void testCreateBasicShelter() throws Exception {
        Session session = Mysql.getSession();
        session.beginTransaction();
        Shelter shelter =  new Shelter("Paws Welcome", format.parse("2013-02-25"));
        session.save(shelter);
        session.getTransaction().commit();
        session.close();
        assertEquals(2, shelter.getId());
        assertEquals(format.parse("2013-02-25"), shelter.getOpened());
    }

    @Test(expected = ConstraintViolationException.class)
    public void testFailsDuplicateShelterName() throws Exception {
        Session session = Mysql.getSession();
        session.beginTransaction();
        Shelter shelter =  new Shelter("Happi Tails", format.parse("2013-02-25"));

        try{
            session.save(shelter);
            session.getTransaction().commit();
        }
        finally {
            session.close();
        }
    }

    @Test
    public void testCreateShelterWithAnimal() throws Exception {
        Session session = Mysql.getSession();
        session.beginTransaction();
        Shelter shelter =  new Shelter("Furry Friends", format.parse("2013-02-25"));
        Animal murphy = new Animal("murphy", 4, "f", format.parse("2015-06-11"), "cat", shelter);
        session.save(murphy);
        session.getTransaction().commit();
        session.close();
    }

    @Test
    public void testGetShelterWithAnimals() throws Exception {
        Session session = Mysql.getSession();
        session.beginTransaction();
        Shelter shelter = session.get(Shelter.class, 1);
//        Animal animal = session.get(Animal.class, 1);
        session.getTransaction().commit();
        session.close();
    }
}
